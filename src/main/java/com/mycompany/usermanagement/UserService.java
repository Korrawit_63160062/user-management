/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.usermanagement;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Acer
 */
public class UserService {

    private static ArrayList<User> userList = new ArrayList<>();

    public static boolean addUser(User user) {
        userList.add(user);
        return true;
    }

    // Overload function
    public static boolean addUser(String username, String password) {
        userList.add(new User(username, password));
        return true;
    }

    public static boolean updateUser(int index, User user) {
        userList.set(index, user);
        return true;
    }

    public static User getUser(int index) {
        if (index > userList.size() - 1) {
            return null;
        }
        return userList.get(index);
    }

    public static ArrayList<User> getUser() {
        return userList;
    }

    public static ArrayList<User> searchUser(String searchText) {
        ArrayList<User> list = new ArrayList<>();
        for (User user : userList) {
            if (user.getUsername().startsWith(searchText)) {
                list.add(user);
            }
        }
        return list;
    }

    public static boolean delUser(int index) {
        userList.remove(index);
        return true;
    }

    public static boolean delUser(User user) {
        userList.remove(user);
        return true;
    }

    public static User login(String userName, String password) {
        for (User user : userList) {
            if (user.getUsername().equals(userName) && user.getPassword().equals(password)) {
                return user;
            }
        }
        return null;
    }

    public static void save() {
        FileOutputStream fos = null;
        try {
            File file = new File("userslist.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(userList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }   
    }

    public static void load() {
                FileInputStream fis = null;
        try {
            File file = new File("userslist.dat");
            fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            userList = (ArrayList<User>) ois.readObject();
            System.out.println(userList);
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fis.close();
            } catch (IOException ex) {
                Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
